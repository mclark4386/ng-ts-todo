export class TodoModel{
    constructor(public title:string = "",public status:string = "started"){}
    
    toggle():void{//mutating models is bad... so don't use this!
        this.status = 
            this.status == "started" ?
            "completed":
            "started"
    }
}