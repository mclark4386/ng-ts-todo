import {Injectable} from 'angular2/core'
import {TodoModel} from './todo-model'

@Injectable()
export class TodoService{
    todos = [
        new TodoModel("eat"),
        new TodoModel("sleep"),
        new TodoModel("code","completed"),
        new TodoModel("play"),
        new TodoModel("record"),
        new TodoModel("sing")
    ];
    
    addTodo(todo:TodoModel){
        this.todos = [...this.todos,todo]
    }
    
    toggleTodo(todo:TodoModel){
        //todo.toggle();//mutates object ... don't do that
        
        const i = this.todos.indexOf(todo);
        
        const status = todo.status == "started" ? "completed":"started";
        const toggledTodo = Object.assign({},todo,{status: status});//should have just done {status} since they are named the same!
        
        this.todos = [
            ...this.todos.slice(0,i),
            toggledTodo,//was todo when we were mutating
            ...this.todos.slice(i+1)
        ]
    }
}