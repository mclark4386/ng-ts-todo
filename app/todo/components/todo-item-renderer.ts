import {Component, Input,Output,EventEmitter} from 'angular2/core'

 //   use encapsulation: ViewEncapsulation.None, to not encapsulate the styles her

@Component({
    selector: 'todo-item-renderer',
    template: `
            <div>
                <style>
                    .completed{
                        text-decoration: line-through;
                    }
                </style>
                <span [ngClass]="todo.status">{{todo.title}}</span>
                <span [contentEditable]="todo.status == 'completed'">{{todo.title}}</span>  
                <span [hidden]="todo.status == 'completed'">{{todo.title}}</span>
                <!-- <button (click)="todo.toggle()">({{todo.status}})</button> -->
                <button (click)="toggle.emit(todo)">({{todo.status}})</button>
            </div>
            `
})
export class TodoItemRenderer{ 
    @Input() todo;
    @Output() toggle = new EventEmitter();
}