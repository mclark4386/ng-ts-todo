import {Component} from 'angular2/core';
import {TodoInput} from './todo/components/todo-input';
import {TodoList} from './todo/components/todo-list';
import {StatusSelector} from './todo/components/status-selector';
import {SearchBox} from './search/components/search-box';

@Component({
    selector: 'my-app',
    directives: [TodoInput,TodoList,StatusSelector,SearchBox],
    template: `<h1>My first angular 2 app {{name}}!</h1>
    <div>
        <input [(ngModel)]="name" placeholder="Name" >
    </div>
    <todo-input>Loading...</todo-input>
    <search-box (term)="term = $event"></search-box><status-selector (select)="status = $event"></status-selector>
    <todo-list [status]="status" [term]="term">Loading...</todo-list>
    `
})
export class AppComponent{
    public name = "world";
}