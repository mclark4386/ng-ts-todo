import {Component,Output,EventEmitter} from "angular2/core"

@Component({
    selector: 'search-box',
    template: `<div class="search">
            <input type="text" #input (input)="term.emit(input.value)" placeholder="Search">
    </div>
    `
})
export class SearchBox{
    @Output() term = new EventEmitter();
    
    ngOnInit(){
        this.term.emit('');
    }
}