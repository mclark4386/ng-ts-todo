import {Component, Inject} from "angular2/core"
import {TodoService} from "../services/todo-service"
import {TodoModel} from "../services/todo-model"

@Component({
    selector: 'todo-input',
    template: `<div>
    <form (submit)="onSubmit()">
    <input type="text" [(ngModel)]="todoModel.title" placeholder="Enter ToDos Here!">
    </form>
    
    
    <!-- just for an example
    <input type="text" #myInput>
    <button (click)="onClick(myInput.value)">Click me</button>
    
    <input type="text" #myInput2>
    <button (mouseover)="onClick2($event,myInput2.value)">Click me 2</button> -->
    </div>`
})
export class TodoInput{
    todoModel:TodoModel = new TodoModel();
    
    constructor(@Inject(TodoService) public todoService){}
    
    onSubmit(){
        this.todoService.addTodo(this.todoModel);
        this.todoModel = new TodoModel();
    }
    
    onClick(value){
        this.todoService.todos.push(value);
    }
    onClick2(event,value){
        this.todoService.todos.push(value);
        console.log(event,value,this.todoService.todos);
    }
}